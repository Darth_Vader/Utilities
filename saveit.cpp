#include <iostream>
#include <sqlite3.h>
#include "header_files/sstring.h"
#include <stdlib.h>
#include "saveitDB_config.h"
#include <time.h>
#include <stdio.h>
#include "header_files/argument_parser.h"
using namespace std;


sqlite3 *db;

vector<argument> args;
vector<string> flags;

string random_substring()
{
	string keys = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	string sub_string = "";
	for(int i=0;i<4;i++)
	{
		sub_string += keys[rand()%(keys.length()-1)];
	}
	return sub_string;
}

string generate_key()
{	
	return random_substring()+"-"+random_substring()+"-"+random_substring();
}

static int insert_callback(void *notUsed, int argc, char** argv, char** azColName)
{
	for(int i=0; i<argc; i++)
	{
		printf("%s = %s\t", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	cout<<endl<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
	return 0;
}

static int last_callback(void *data, int argc, char** argv, char** azColName)
{
	cout<<"\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	cout<<"\n-- "<<argv[0]<<" ----------------- "<< argv[1]<<endl; 
	
	if( argv[2] != NULL )
	{
		cout<<"\n-- "<< argv[2] <<"\n";
	}
	cout<<endl<<endl<<argv[3]<<endl;
	cout<<"\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	return 0;
}

void help()
{
	cout<<"\nsaveit [option]\n";
	cout<<"\t--create_table\n";
	cout<<"\t--new\n";
	cout<<"\t--raw\n";
	cout<<"\t--last\n";
	cout<<"\t--recent=[count]\n";
}

int main(int argc,char* argv[])
{
	
	srand(time(0));

	if(argc == 1)
	{
		help();
		exit(0);
	}

	char *error_msg = 0;

	// opening database
	if( sqlite3_open(DATABASE_NAME, &db) )
	{
		cout<<"\nCan't open database :"<<DATABASE_NAME<<endl;
		exit(0);
	}


	flags = parse_flags(argv,argc);
	args = parse_arguments(argv,argc);

	if( check_flag(flags,"new") )
	{
		// new object
		string data, description;

		cout<<"\nText :\n";
		getline(cin,data);
		cout<<"\n------------------------------------\nDescription : ";
		getline(cin,description);

		string key = generate_key();


		string query = "insert into " + TABLE_NAME + "(id,data,description) values ('" + key + "','" + data + "','" + description + "');";

		if( sqlite3_exec(db, query.c_str(), insert_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError: "<<error_msg<<endl;
			sqlite3_free(error_msg);
			exit(0);
		}
		else
		{
			cout<<"\nRecords created successfully."<<endl;
		}		

	}
	else if( check_flag(flags,"raw"))
	{
		string data;
		cout<<"\nGive raw query :-\n";
		getline(cin,data);
		
		cout<<"\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

		if( sqlite3_exec(db, data.c_str(), insert_callback,0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError : "<<error_msg<<endl;
			sqlite3_free(error_msg);
			exit(0);
		}

	}
	else if( check_flag(flags,"create_table"))
	{
		string query = "CREATE TABLE saved_objects(\
		id char(30) primary key not null,\
		data char(200) not null,\
		description char(50),\
		timestamp DATE DEFAULT (datetime('now','localtime'))\
		);";

		if( sqlite3_exec(db, query.c_str(), insert_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError: "<<error_msg<<endl;
			sqlite3_free(error_msg);
			exit(0);
		}
		else
		{
			cout<<"\nTable created successfully."<<endl;
		}	
	
	}
	else if( check_flag(flags,"last")  )
	{
		string query = "select max(timestamp), id, description, data from " + TABLE_NAME + ";";
		if( sqlite3_exec(db, query.c_str(), last_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError : "<<error_msg<<endl;
			sqlite3_free(error_msg);
			exit(0);
		}
	}
	else if( value_of(args,"recent") != "" )
	{
		string query = "select timestamp, id, description, data from " + TABLE_NAME + " order by timestamp desc limit " + value_of(args,"recent") + ";";
		if( sqlite3_exec(db, query.c_str(), last_callback,0, &error_msg) != SQLITE_OK)
		{
			cout<<"\nError : "<<error_msg<<endl;
			sqlite3_free(error_msg);
			exit(0);
		}
	}

	// closing database
	sqlite3_close(db);

}
