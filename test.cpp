#include <iostream>
#include "header_files/argument_parser.h"

using namespace std;

int main(int argc,char *argv[])
{
	vector<argument> x = parse_arguments(argv,argc);
	for(int i=0; i<x.size(); i++)
	{
		cout<<x[i].name<<" "<<x[i].value<<endl;
	}
	return 0;
}
