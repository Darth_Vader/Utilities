#include <cstring>
#include <iostream>
#include <vector>
using namespace std;

struct argument
{
	string name;
	string value;	
	argument(string name,string value)
	{
		this->name = name;
		this->value = value;
	}
	argument(char* name, char* value)
	{
		this->name = string(name);
		this->value = string(value);
	}
	argument(string name,char* value)
	{
		this->name = name;
		this->value = string(value);
	}
};

vector<string> parse_options(char* argv[],int argc)
{
	vector<string> options;
	string option;

	for(int i=1; i<argc; i++)
	{
		option = string(argv[i]);
		if( option.size() >=1 )
		{
			
			if( option[0] == '-' || option.find('=') < option.length())
				continue;
		}
		options.push_back(option);
	}

	return options;
}

vector<string> parse_flags(char *argv[],int argc)
{
	vector<string> flags;
	char *flag;
	int idx1,idx2;

	flag = new char[100];

	for(int i=1; i<argc; i++)
	{
		if( strlen(argv[i]) > 2 && argv[i][0] == '-' && argv[i][1] == '-' && argv[i][2])
		{
			string temp = string(argv[i]).substr(2,strlen(argv[i]));
			if( temp.find("=") != string::npos)
				continue;		// continue loop
			idx1 = idx2 = 0;
			while( idx1 < temp.length())
			{
				flag[idx2++] = temp[idx1++];
			}

			flag[idx2] = '\0';

			if( strlen(flag) >0)
			{
				flags.push_back(flag);
			}

		}
	}

	delete [] flag;

	return flags;
}

bool check_flag(vector<string> flags,string flag)
{
	for(int i=0; i<flags.size(); i++)
	{
		if(flags[i] == flag)
			return true;	
	}
	return false;
}

vector<argument> parse_arguments(char* argv[],int argc)
{
	vector<argument> parsed_arguments;
	
	parsed_arguments.push_back(argument("program_name",string(argv[0])));

	char *name, *value;
	int idx1,idx2;

	name = new char[100];
	value = new char[100];

	for(int i=1; i<argc; i++)
	{
		if( strlen(argv[i]) > 2 && argv[i][0] == '-' && argv[i][1] == '-')
		{
			string temp = string(argv[i]).substr(2,strlen(argv[i]));
			name[0] = value[0] = '\0';
			idx1 = idx2 = 0;
			while( idx1 < temp.length() && temp[idx1] != '=')
			{
				name[idx2++] = temp[idx1++];
			}

			idx1++;

			name[idx2] = '\0';
			idx2 = 0;

			while( idx1 < temp.length())
			{
				value[idx2++] = temp[idx1++];
			}

			value[idx2] = '\0';

			if( strlen(value) >0 && strlen(name) > 0)
			{
				parsed_arguments.push_back(argument(name,value));
			}

		}
	}

	delete []  name;
	delete [] value;

	return parsed_arguments;

}

string value_of(vector<argument> args, string name)
{
	for(int i=0; i<args.size(); i++)
	{
		if(args[i].name == name)
			return args[i].value;
	}

	return "";
	
}
