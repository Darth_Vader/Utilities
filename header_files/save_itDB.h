#include <sqlite3.h>

#define ERROR -1
#define SUCCESS 0

int connect_to_db(const char* db_file_name,  sqlite3 *&db_pointer)
{
	int rc = sqlite3_open(db_file_name, &db_pointer);
	if( rc )
		return ERROR;
	else
		return SUCCESS; 
}

int close_db( sqlite3 *& db_pointer)
{
	return sqlite3_close(db_pointer);
}
