#include <iostream>
#include <cstring>

using namespace std;

std::string sreplace(std::string main_string,std::string word, std::string replacement)
{
	for(int i=0; i<main_string.length(); i++)
	{
		if( main_string[i] == word[0] )
		{
			int j = i+1,k=1;
			while( j<main_string.length() && k< word.length() && word[k] == main_string[j])
			{
				j++;
				k++;
			}

			if( k == word.length() )
			{
				// pattern matched
				return main_string.substr(0,i)+ replacement + main_string.substr(i+k,main_string.length());
			}
		}
	}

	return main_string;

}

void s_to_lower(string &str)
{
	for(int i=0; i<str.length(); i++)
	{
		str[i] = tolower(str[i]);
	}
}

string join_path(string home_path,string directory)
{
	return string(home_path+ directory);
}
