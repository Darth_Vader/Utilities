#include <iostream>
#include <sqlite3.h>
#include "header_files/sstring.h"
#include <stdlib.h>
#include "wordsDB_config.h"
#include <stdio.h>
#include <fstream>
#include "header_files/argument_parser.h"
using namespace std;


sqlite3 *db;

vector<string> args;
vector<string> flags;
vector<argument> special_args;

void help()
{
	cout<<"\nUsage : words [OPTION] <FLAG>"<<endl;
	cout<<"\nFlags :"<<endl;
	cout<<"\t\t--initial_setup for first time run <creating database and table>."<<endl;
	cout<<"\t\t--case for retaining case sensitive characters in word."<<endl;
	cout<<"\t\t--random=<count> -- will take out random 'count' number of words. By default 10 words will be retreived."<<endl;
	cout<<"\t\t--file=<file_path> -- will insert all words for the specified file."<<endl;
	cout<<"\nOptions : "<<endl;
	cout<<"\t\t [word] - will be converted to lower case letters."<<endl;
}

void error()
{
	cout<<"\nExecute : 'words --help' for help."<<endl;
	exit(0);
}

static int insert_callback(void *notUsed, int argc, char** argv, char** azColName)
{
	for(int i=0; i<argc; i++)
	{
		printf("%s = %s\t", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	cout<<endl<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
	return 0;
}

static int last_callback(void *data, int argc, char** argv, char** azColName)
{
	// just displaying word
	cout<<argv[0]<<endl;
	return 0;
}

int main(int argc,char* argv[])
{
	char *error_msg = 0;
	int new_inserted_words_count = 0;
	bool inserted = false;

	flags = parse_flags(argv,argc);
	args = parse_options(argv,argc);
	special_args = parse_arguments(argv,argc);

	if( check_flag(flags,"help") || argc == 1)
	{
		help();
		exit(0);
	}
	
	if( check_flag(flags,"initial_setup"))
	{
		cout<<"\nSetting up database ..."<<endl;
		if( sqlite3_open(join_path(HOME_DIRECTORY,DATABASE_NAME).c_str(),&db))
		{
			cout<<"\n[ERROR] Can't open database. Database should be created automatically now if not existed earlier."<<endl;
		}
		else
			cout<<"\n[OK] Database already exists."<<endl;
		cout<<"Creating Table in database ..."<<endl;

		string query = "CREATE TABLE " + TABLE_NAME + "(\
		word char(30) primary key not null,\
		timestamp DATE DEFAULT (datetime('now','localtime'))\
		);";

		if( sqlite3_exec(db, query.c_str(), insert_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"[ERROR] Table creation failed with error :\n"<<error_msg<<endl;
			sqlite3_free(error_msg);
			exit(0);
		}
		else
		{
			cout<<"[OK] Table created successfully."<<endl;
		}		
	}

	// opening database
	if( sqlite3_open(join_path(HOME_DIRECTORY, DATABASE_NAME).c_str(), &db) )
	{
		cout<<"\nCan't open database :"<<DATABASE_NAME<<endl;
		error();
	}

    bool operation_performed = false;

	if( check_flag(flags,"case"))
	{
		if( args.size() == 0)
		{
			cout<<"\nNo words provided to save."<<endl;
			error();
		}
		// not coverting word to lower case
	}
	else
	{
		// converting all words to lower case before saving
		for( int i=0; i<args.size(); i++)
		{
			s_to_lower(args[i]);
		}
	}


	if(check_flag(flags,"all"))
	{
		//display all words
		string query = "select word from " + TABLE_NAME + ";";
		if( sqlite3_exec(db, query.c_str(), last_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError : "<<error_msg<<endl;
			sqlite3_free(error_msg);
			error();
		}
        operation_performed = true;
	}

	if(check_flag(flags,"random"))
	{
		//display 10 random words from database
		string limit = "10";
		if( value_of(special_args,"count") != "")
			limit = value_of(special_args,"count");
	
		string query = "select word from " + TABLE_NAME + " order by random() limit " + limit + ";";
		if( sqlite3_exec(db, query.c_str(), last_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError : "<<error_msg<<endl;
			sqlite3_free(error_msg);
			error();
		}	
        operation_performed = true;
	}

	if( value_of(special_args,"random") != "")
	{
		// --random=30
		string limit = value_of(special_args,"random");
		if( value_of(special_args,"count") != "")
			limit = value_of(special_args,"count");
	
		string query = "select word from " + TABLE_NAME + " order by random() limit " + limit + ";";
		if( sqlite3_exec(db, query.c_str(), last_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError : "<<error_msg<<endl;
			sqlite3_free(error_msg);
			error();
		}	
        operation_performed = true;
	}

	if( value_of(special_args,"file") != "")
	{
		string path = value_of(special_args,"file");	
		ifstream fin;
		fin.open(path);	
		while( fin )
		{
			string word;
			fin>>word;
			if( !fin)
				break;
			string query = "insert into " + TABLE_NAME + "(word) values ('" + word + "');";
			if( sqlite3_exec(db, query.c_str(), insert_callback, 0, &error_msg) != SQLITE_OK )
			{
				cout<<"[ERROR] Query execution error for word : "<<word<<"\n\t\tError is : "<<error_msg<<endl;
				sqlite3_free(error_msg);
			}
			else
			{
				cout<<"[OK] Inserted : "<<word<<endl;
				new_inserted_words_count++;
				inserted = true;
			}
		}
		fin.close();
	}

	if( value_of(special_args,"delete") != "")
	{
		string query = "delete from " + TABLE_NAME + " where word = '" + value_of(special_args,"delete") + "';";
		if( sqlite3_exec(db, query.c_str(), last_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"\nError : "<<error_msg<<endl;
			sqlite3_free(error_msg);
			error();
		}	
        operation_performed = true;
	}

	for(int i=0; !operation_performed && i<args.size(); i++)
	{
		string query = "insert into " + TABLE_NAME + "(word) values ('" + args[i] + "');";
		if( sqlite3_exec(db, query.c_str(), insert_callback, 0, &error_msg) != SQLITE_OK )
		{
			cout<<"[ERROR] Query execution error for word : "<<args[i]<<"\n\t\tError is : "<<error_msg<<endl;
			sqlite3_free(error_msg);
		}
		else
		{
			cout<<"[OK] Inserted : "<<args[i]<<endl;
			new_inserted_words_count++;
			inserted = true;
		}
	}

	if( inserted )
		cout<<"[...] Insertion operation completed. Inserted "<<new_inserted_words_count<<" new words."<<endl;

	// closing database
	sqlite3_close(db);

}
