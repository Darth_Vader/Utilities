#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <iostream>
#include <arpa/inet.h>
#include <linux/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include "percent_bar.h"

using namespace std;

int socket_id = -1;
char server_ip[16];
int port = -1;

struct file_info
{
	int size;	// in bytes
	char *file_name;
};

#define chunck_size 5120

file_info extract_info(char *message)
{
	file_info fi;
	char *temp = new char[strlen(message)];
	int i = 0;
	int length = strlen(message);
	while( message[i] != '|')
	{
		temp[i] = message[i++];
	}
	temp[i] = '\0';
	
	fi.size = atoi(temp);
	

	int j = 0;
	i++;
	while( message[i] !='|')
	{
		temp[j++] = message[i++];
	}

	temp[j] = '\0';

	
	fi.file_name = new char[strlen(temp)];
	strcpy(fi.file_name,temp);
	return fi;

}

int main(int argc,char* argv[])
{
	if(argc == 1)
	{
		port = 9998;
		strcpy(server_ip,"127.0.0.1");
	}
	if(argc == 2)
	{
		strcpy(server_ip,"127.0.0.1");
		port = atoi(argv[1]);
	}
	else if( argc >= 3)
	{
		strcpy(server_ip,argv[2]);
		port = atoi(argv[1]);
	}

	if( ( socket_id = socket ( AF_INET, SOCK_STREAM, 0 ) ) < 0 )
	{
		cout<<"\nError : Failed to create socket "<<socket_id<<endl;
		exit(0);	
	}

	sockaddr_in server_addr;
	socklen_t addrlen;
	char buffer[chunck_size];
	char file_path[1024];

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(server_ip);
	server_addr.sin_port = htons(port);

	int err;
	cout<<"\nServer :\t"<<server_ip<<":"<<port;
	cout<<"\nConnecting...........\n";
	if( ( err =  connect( socket_id, (sockaddr*) & server_addr, sizeof(server_addr))) < 0)
	{
		cout<<"\nError : Failed to connect to "<<server_ip<<":"<<port<<" : "<<err<<endl;
		exit(0);
	}
	
	cout<<"\nConnected to : "<<server_ip<<":"<<port<<endl;

	cout<<"\nWaiting for server's signal ..."<<endl;

	read( socket_id, buffer, 1024);

	/*
		Extracting filename, size from message
	*/
	
	file_info fi = extract_info(buffer);

	cout<<"\nGot Signal ......";
	cout<<"\nFile size : "<<fi.size<<endl<<"File name : "<<fi.file_name;
	cout<<"\nEnter path for the file ( including file name and make sure directories exists ) : ";
	cin>>file_path;

	int fd = fileno(fopen( file_path, "wb"));

	if( fd < 0 )
	{
		cout<<"\nError in opening "<<file_path<<endl;
		exit(0);
	}

	
	cout<<"\nReady to receive file ....\n";

	bar_t bar;
	bar.total_amount = fi.size;
	bar.set_event_name(string(file_path));


	int received_bytes = 0;
	int bytes = -1;
	while( received_bytes <= fi.size )
	{
		bytes = read(socket_id, buffer, chunck_size);
		received_bytes += bytes;
		bar.update((float)(received_bytes));
		if( bytes <= 0 )
		{
			break;
		}
		write(fd,buffer,bytes);
	
	}

	cout<<"\nSuccessfully received\n";

	close(fd);
	close(socket_id);

	return 0;
}
