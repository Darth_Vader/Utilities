#include <cstring>
#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

#define max_bar_length 50			// 50 characters
#define progress_bar_character '|'

string string_time(double sec)
{
	int mins = sec/60;
	int secs = sec - mins*60;
	string time_ = "";
	if( mins < 10)
		time_ ="0";
	time_ += to_string(mins)+":";
	if(secs < 10 )
		time_ += "0";
	time_ += to_string(secs);
	return time_;
}

struct bar_t
{
	float total_amount;
	string event_name;
	time_t start_time;
	string finished_message;

	bar_t()
	{
		this->start_time = time(0);
		finished_message ="Event Completed";
	}
	
	bar_t(float total_amount,string event_name)
	{
		this->total_amount = total_amount;
		this->event_name = event_name;
		finished_message = "Event Completed";
	}

	void set_finished_message(string message)
	{
		this->finished_message = message.substr(0,10) + "...";
	}
	
	void set_event_name(string event_name)
	{
		this->event_name = event_name.substr(0,10) + "...";
	}
	int bar_length(float amount_done)
	{
		return (((float) (amount_done)/(total_amount))* max_bar_length);
	}

	bool update(float p)
	{
		int length = bar_length(p);

		time_t now = time(0);
		double sec = difftime(now,this->start_time);

		cout<<"\r";
		cout<<this->event_name<<"    ";
		for(int i=0;i<length;i++)
			cout<< progress_bar_character;
		for(int i=length; i< max_bar_length; i++)
			cout<<".";

		cout<<"    ["<<p<<"/"<<this->total_amount<<"] "<<(float)(p/this->total_amount*100)<<"%    "<<string_time(sec);
		for(int i=0;i<5;i++)
			cout<<" ";
		if(p == this->total_amount)
			cout<<"\t"<<this->finished_message;
		//cout<<endl;
		//cout << "\x1b[A";
		
		return p == this->total_amount;
	}

};
