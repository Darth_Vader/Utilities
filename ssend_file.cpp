#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <iostream>
#include <arpa/inet.h>
#include <linux/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "percent_bar.h"

using namespace std;

int socket_id = -1;
char server_ip[16];
int port = -1;

#define chunck_size 5120

int main(int argc,char* argv[])
{
	if(argc == 1)
	{
		port = 9998;	// default sender's port
		strcpy(server_ip,"127.0.0.1");
	}
	if(argc == 2)
	{
		strcpy(server_ip,"127.0.0.1");
		port = atoi(argv[1]);
	}
	else if( argc >= 3)
	{
		strcpy(server_ip,argv[2]);
		port = atoi(argv[1]);
	}

	if( ( socket_id = socket ( AF_INET, SOCK_STREAM, 0 ) ) < 0 )
	{
		cout<<"\nError : Failed to create socket "<<socket_id<<endl;
		exit(0);	
	}

	sockaddr_in server_addr;
	socklen_t addrlen;
	char buffer[chunck_size];
	char file_path[1024];

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(server_ip);
	server_addr.sin_port = htons(port);

	int err;
	cout<<"\nServer :\t"<<server_ip<<":"<<port;
	cout<<"\nStarting ...........\n";
	if( ( err =  bind( socket_id, (sockaddr*) & server_addr, sizeof(server_addr))) < 0)
	{
		cout<<"\nError : Failed to bind ...  "<<server_ip<<":"<<port<<" : "<<err<<endl;
		exit(0);
	}

	if( (err = listen(socket_id,1)) < 0)
	{
		cout<<"\nError: Failed to listen ... "<<server_ip<<":"<<port<<" error code =  "<<err<<endl;
		exit(0);
	}
	
	cout<<"\nListening to : "<<server_ip<<":"<<port<<endl;

	cout<<"\nEnter file path : ";
	cin.getline(file_path,1024);

	struct stat st;	

	if ( stat(file_path, &st) < 0)
	{
		cout<<"\nError file doesn't exists or wrong path. ? = "<<file_path<<endl;
		exit(0);
	}
	// Preparing message
	char file_name[20];
	cout<<"\nEnter file name to show on receiver side : ";
	cin>>file_name;


	char *str = new char[1024];
	sprintf(str,"%ld",st.st_size);
	strcat(str,"|");
	strcat(str,file_name);
	strcat(str,"|");

	cout<<"\nSent meta data is : "<<str<<endl;

	cout<<"Waiting for client .........\n";

	int client_socket = accept(socket_id,NULL,NULL);

	if( client_socket < 0)
	{
		cout<<"\nError: failed to accept client. ? "<<client_socket<<endl;
		exit(0);
	}

	cout<<"\nSending file meta data ......";
	write(client_socket,str,strlen(str)+1);
	cout<<"\nMeta Data sent successfully.";
	cout<<"\nPreparing file for sending..."<<endl;

	int fd = fileno(fopen( file_path, "rb"));

	if( fd < 0 )
	{
		cout<<"\nError in opening "<<file_path<<endl;
		exit(0);
	}

	
	cout<<"\nReady to send file ....\n";

	int sent_bytes = 0;
	int bytes = -1;

	bar_t bar;
	bar.total_amount = st.st_size;
	bar.set_event_name( string(file_path));

	while( sent_bytes <= st.st_size )
	{	
		bytes = read(fd,buffer,chunck_size);

		if( bytes <= 0)
			break;

		if( write(client_socket, buffer, bytes) < 0)
		{
			cout<<"\nError in sending data to client.\n";
			exit(0);
		}
		sent_bytes += bytes;
		bar.update((float)(sent_bytes));
	
	}

	cout<<"\nSending operation finished.\n";

	close(fd);
	close(socket_id);
	close(client_socket);

	return 0;
}
